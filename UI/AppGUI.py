from PySide import QtGui, QtUiTools, QtCore
from __builtin__ import isinstance

from Connectors import ShodanConnector as sc
from Connectors import MongoConnector as mc
from pprint import pformat


def loadUiWidget(uifilename, parent=None):
    loader = QtUiTools.QUiLoader()
    uifile = QtCore.QFile(uifilename)
    uifile.open(QtCore.QFile.ReadOnly)
    ui = loader.load(uifile, parent)
    uifile.close()
    return ui

class AppGUI(QtGui.QMainWindow):
    def __init__(self):
        super(AppGUI, self).__init__()
        self.queries = {}
        self.mc = mc.MongoConnector("configs/mongo.cfg")
        self.sc = sc.ShodanConnector()
        self.setWindowTitle("PyShodan")
        self.widget = loadUiWidget("forms/app.ui", self)
        self.setWindowIcon(QtGui.QIcon('forms/pyshon.png'))
        self.searchBar = QtGui.QLineEdit(self)
        self.textView = self.widget.textEdit
        self.textViewMongo = self.widget.textEdit_mongo
        self.queriesBox = self.widget.queriesBox
        self.tabs = self.widget.tabs
        self.status = self.widget.status_printer
        self.status.setText("On Hold")
        self.textViewStatistics = self.widget.textEdit_statistics
        self.queriesBox.setLineEdit(self.searchBar)
        self.searchButton = self.widget.searchButton
        self.save2dbButton = self.widget.save2dbButton
        self.ipbutton = self.widget.ipqueryButton
        self.countrybutton = self.widget.countryButton
        self.queriesCountButton = self.widget.queriesCountButton
        self.geoButtton = self.widget.geoButton
        self.portButton = self.widget.portButton
        self.justIpButton = self.widget.justIpButton
        self.hostButton = self.widget.hostButton
        self.setCentralWidget(self.widget)
        self.fillQueriesBox()
        self.connectAll()
        self.show()

    def connectAll(self):
        self.searchBar.returnPressed.connect(self.performSearch)
        self.searchButton.clicked.connect(self.performSearch)
        self.save2dbButton.clicked.connect(self.saveToDb)
        self.ipbutton.clicked.connect(self.showIpDialog)
        self.countrybutton.clicked.connect(self.showCountryDialog)
        self.queriesCountButton.clicked.connect(self.queriesCount)
        self.geoButtton.clicked.connect(self.showGeoDialog)
        self.portButton.clicked.connect(self.showPortDialog)
        self.justIpButton.clicked.connect(self.justIpSearch)
        self.hostButton.clicked.connect(self.showHostDialog)


    def fillQueriesBox(self):
        self.queriesBox.addItems(self.mc.loadDBQueries())

    def performSearch(self):
        index = self.tabs.currentIndex()
        if index == 0:
            self.searchShodan()
        elif index == 1:
            self.searchMongo()
        self.status.setText("Done")

    def searchMongo(self):
        query = self.searchBar.text()
        result = self.mc.loadFromDB(query)
        if result is not None:
            self.textViewMongo.setText(self.sc.stringfyQuery(result["results"]))
        else:
            self.textViewMongo.setText("No results found.")

    def searchShodan(self):
        self.status.setText("Searching")
        query = self.searchBar.text()

        if not query:
            self.textView.setText("")
            self.status.setText("No Input")
        else:
            result,_  = self.sc.query(query)

            if not result:
                self.textView.setText("No results found.")
            else:
                for e in result:
                    e["ip"] = e["ip_str"]

                self.queries[query] = {"query":query, "results":result}
                if result is not None:
                    self.textView.setText(self.sc.stringfyQuery(result))
                else:
                    self.textView.setText("No results found.")

    def saveToDb(self):
        query = self.searchBar.text()
        if self.queries.has_key(query):
            try:
                self.mc.saveToDB(self.queries[query])
                self.status.setText("Upserted")
            except OverflowError:
                self.textView.setText("Mongo can not handle long-type integers.")
        else:
            self.textView.setText("This query has not been executed yet.")

    def showIpDialog(self):
        ip, ok = QtGui.QInputDialog.getText(self, 'Input Ip',
            'Enter a valid IP:')
        if ok:
            self.searchIp(ip)

    def searchIp(self, ip):
        res = self.mc.getResultsByIP(ip)
        if res:
            self.textViewStatistics.setText(pformat(res, indent=4))

    def showHostDialog(self):
        ip, ok = QtGui.QInputDialog.getText(self, 'Input Ip',
            'Enter a valid IP:')
        if ok:
            self.searchHost(ip)

    def searchHost(self, ip):
        self.status.setText("Host Search")
        res = self.sc.lookupHost(ip)
        if res:
            self.status.setText("Done")
            self.textView.setText(pformat(res, indent=4))
        else:
            self.status.setText("No host matches")

    def showCountryDialog(self):
        country, ok = QtGui.QInputDialog.getText(self, 'Input Country',
            'Enter a valid Country Name:')
        if ok:
            self.searchCountry(country)

    def searchCountry(self, country):
        res = self.mc.getResultsByCountry(country)
        if res:
            self.textViewStatistics.setText(pformat(res, indent=4))

    def queriesCount(self):
        count = self.mc.getQueryCount()
        queries = pformat(self.mc.loadDBQueries(), indent=4)
        res = "Total Queries: {}\n".format(count)
        res += queries
        self.textViewStatistics.setText(res)

    def showGeoDialog(self):
        data, ok = QtGui.QInputDialog.getText(self, 'Input GeoLocation Data',
            'Data format {%f[longitude] %f[latitude] %f[maxDistance]}')
        if ok:
            self.searchGeo(*map(float, data.split(" ")))

    def searchGeo(self, longitud, latitud, maxDistance):
        res = self.mc.getNearResultsByLocation(longitud, latitud, maxDistance)
        if res:
            self.textViewStatistics.setText(pformat(res, indent=4))

    def showPortDialog(self):
        data, ok = QtGui.QInputDialog.getText(self, 'Input Port Info',
            'Data format {%d[portNum] %s[tcp/udp]}:')
        if ok:
            port, tcp_udp = data.split(" ")
            self.searchPort(int(port), tcp_udp.lower())

    def searchPort(self, port, tcp_udp):
        res = self.mc.getResultsByPort(port, tcp_udp)
        if res:
            self.textViewStatistics.setText(pformat(res, indent=4))

    def justIpSearch(self):
        self.status.setText("Searching")
        res = self.sc.getIPs(self.searchBar.text())
        if res:
            self.status.setText("Done")
            self.textView.setText(pformat(res, indent=4))
        else:
            self.status.setText("No matches")





def loadStyleSheet():
    with open("forms/style.qss", "r") as f:
        return f.read()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    app.setStyleSheet(loadStyleSheet())
    widgget = AppGUI()
    sys.exit(app.exec_())
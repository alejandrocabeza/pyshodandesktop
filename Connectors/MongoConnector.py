import json

from pymongo import MongoClient

from pymongo import ASCENDING
from pymongo import GEOSPHERE

from operator import itemgetter

class MongoConnector:

    def __init__(self, path):
        self.applyConfig(self.getConfig(path))
        self.client = MongoClient(self.url)
        self.db = self.client[self.cli]
        self.db.authenticate(self.user, self.pwd)

    def saveToFile(self, json, filename):
        with open(filename, "w") as f:
            f.write(json)

    def loadFromFile(self, filename):
        with open(filename, "r") as f:
            return json.loads(f.read())

    def saveToDB(self, json, database = "queryresults"):
        self.db[database].update({"query" : json["query"]}, json, upsert = True)
        for e in json["results"]:
            d = {"loc"     : { "type"       : "Point",
                               "coordinates": [e["location"]["longitude"], e["location"]["latitude"]]},
                 "query"   : json["query"],
                 "ip"      : e["ip_str"]}
            self.db["geoIp"].insert_one(d)

    def loadFromDB(self, query, db = "queryresults"):
        result = self.db[db].find_one({"query":query})
        return result

    def _reset(self, db = "queryresults", index = "query", type=ASCENDING):
        self.db[db].remove()
        if db == "geoIp":
            self.db[db].create_index([("ip", ASCENDING)])
            self.db[db].create_index([("loc", GEOSPHERE)])
        else:
            self.db[db].create_index([(index, type)], unique=True)



    def getConfig(self, fname):
        with open(fname, "r") as f:
            return {k:v.strip() for k,v in (x.split("=") for x in f.readlines())}

    def applyConfig(self, data):
        self.__dict__.update(data)

    '''
    MONGODB QUERIES
    '''

    def getQueryCount(self, db = "queryresults"):
        return self.db[db].count()

    def loadDBQueries(self, db = "queryresults"):
        result = self.db[db].find({}).distinct(key="query")
        result.sort()
        return result

    def getResultsByIP(self, ip, db = "queryresults"):
        return set(map(itemgetter("query"), self.db[db].aggregate(
            [
                {'$project' : {'_id': 0, 'query': 1, 'results': 1}},
                {'$unwind'  : '$results'},
                {'$match'   : {'results.ip_str' : ip}},
                {'$project' : {'query': 1}}
            ]
        )))

    def getResultsByCountry(self, country, db = "queryresults"):
        return set(map(itemgetter("query"), self.db[db].aggregate(
            [
                {'$project' : {'_id': 0, 'query': 1, 'results': 1}},
                {'$unwind'  : '$results'},
                {'$match'   : {'results.location.country_name' : country}},
                {'$project' : {'query': 1}}
            ]
        )))

    def getResultsByPort(self, portNumber, portType = "tcp", db = "queryresults"):
        return set(map(itemgetter("query"), self.db[db].aggregate(
            [
                {'$project' : {'_id': 0, 'query': 1, 'results': 1}},
                {'$unwind'  : '$results'},
                {'$match'   : {'results.port' : portNumber, 'results.transport' : portType}},
                {'$project' : {'query': 1}}
            ]
        )))

    def getNearResultsByLocation(self, longitude, latitude, maxDistance=5000000):
        return map(lambda x: (x["query"], x["ip"]),self.db["geoIp"].find({"loc":{"$near": {"$maxDistance":maxDistance ,'$geometry' : { 'type': 'Point' , 'coordinates':[longitude, latitude]}}}}))




if __name__ == "__main__":
    print "MongoConnector"
    mc = MongoConnector("../UI/configs/mongo.cfg")
    # mc._reset("geoIp")
    # mc._reset("queryresults")

    from pprint import pprint

    asd = mc.getNearResultsByLocation(150, 80)


    for element in asd:
        pprint(element)
    print "COUNT: " + str(len(asd))
    print "END"
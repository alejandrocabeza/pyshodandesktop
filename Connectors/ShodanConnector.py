import shodan
from operator import itemgetter


class ShodanConnector:

    SHODAN_API_KEY = "sxEXi8IslxlwVBl4dYykYOMgpTFyjVs1"

    FACETS = ['org',
              'domain',
              'port',
              'asn',
             ('country', 5),]

    FACET_TITLES = {'org':     'Top 10 Organizations',
                    'domain':  'Top 10 Domains',
                    'port':    'Top 10 Ports',
                    'asn':     'Top 10 Autonomous Systems',
                    'country': 'Top 5 Countries',}

    def __init__(self):
        self.api      = shodan.Shodan(self.SHODAN_API_KEY)
        self.APIError = shodan.APIError

    def query(self, query):
        try:
            results = self.api.search(query)
            return (results["matches"], results["total"])
        except self.APIError, e:
            print "Query Error: %s" % e
            return (None, None)

    @staticmethod
    def printQuery(query):
        for result in query:
            print "IP: %s" % result["ip_str"]
            print result["data"]
            print ""

    @staticmethod
    def stringfyQuery(query):
        ret = ""
        for result in query:
            ret += "IP: %s\n" % result["ip_str"]
            ret += result["data"]+"\n"
        return ret

    def lookupHost(self, ip):
        try:
            return self.api.host(ip)

        except self.APIError, e:
            print "Lookup Error: %s" % e
            return None

    @staticmethod
    def printHost(lookedupHost):
        print """
        IP: %s
        Organization: %s
        Operating System: %s
              """ % (lookedupHost['ip_str'], lookedupHost.get('org', 'n/a'), lookedupHost.get('os', 'n/a'))

        # Print all banners
        for item in lookedupHost['data']:
            print """
            Port: %s
            Banner: %s
                  """ % (item['port'], item['data'])

    def getIPs(self, query):
        try:
            return map(itemgetter('ip_str'), self.api.search(query)['matches'])

        except self.APIError, e:
            print "IPs Error: %s" % e
            return None

    @staticmethod
    def printIPs(results):
        for service in results:
                print service['ip_str']

    def getFacet(self, query):
        try:
            return self.api.count(query, facets=self.FACETS)

        except self.APIError, e:
            print "Facet Error: %s" % e
            return None
    @staticmethod
    def printFacet(self, query):
        print 'Shodan Summary Information'
        print 'Query: %s' % query
        print 'Total Results: %s\n' % query['total']

        for facet in query['facets']:
            print self.FACET_TITLES[facet]
            for term in query['facets'][facet]:
                print '%s: %s' % (term['value'], term['count'])
            print ''

    @staticmethod
    def stringfyFacet(self, query):
        ret = ""
        ret += 'Shodan Summary Information\n'
        ret += 'Query: %s\n' % query
        ret += 'Total Results: %s\n' % query['total']

        for facet in query['facets']:
            ret += "%s\n" % str(self.FACET_TITLES[facet])
            for term in query['facets'][facet]:
                ret += '%s: %s\n' % (term['value'], term['count'])


if __name__ == "__main__":
    c = ShodanConnector()
    #q, t =  c.query("netgear")
    #c.printQuery(q)

    #look = c.lookupHost("83.14.0.34")
    #c.printHost(look)

    #ips = c.getIPs("netgear")
    #c.printIPs(ips)

    facet = c.getFacet("netgear")
    c.printFacet(facet)
